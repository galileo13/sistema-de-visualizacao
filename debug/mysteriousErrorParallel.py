import os
import re
import numpy as np
from numpy import arange,array,ones
import pandas as pd
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import time 
from joblib import Parallel, delayed
from sys import argv
import csv
import matplotlib.ticker as plticker

"""
Example of running:

python3 mysteriousErrorParallel.py
"""

# some global vars
okGalaxies = []
errorGalaxies = []
galaxiesWithCoreFile = []
galaxiesWithTimelimit = []

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

# clear files array
def getProcessedFolders(path, startName):
    items = os.listdir(path)

    newlist = []
    for names in items:
        if names.startswith(startName):
            newlist.append(names)

    return newlist

# funcao que ordena as pastas nas pastas das galaxias processadas, pois linux quebra ordem dos Runs
def catalogSort(t):
    if t[0]=='c':
    	temp = re.search('catalogOPL1(.*)', t) # ordena usando digitos que vem depois de Run e antes de _58, que eh o numero de Run
    	return int(temp.group(1))

# funcao que ordena as pastas nas pastas das galaxias processadas, pois linux quebra ordem dos Runs
def processedSort(t):
    if t[0]=='p':
    	temp = re.search('processed(.*)', t) # ordena usando digitos que vem depois de Run e antes de _58, que eh o numero de Run
    	return int(temp.group(1))

def runSort(t):
    if t[0]=='R':
    	temp = re.search('Run(.*)_58', t) # ordena usando digitos que vem depois de Run e antes de _58, que eh o numero de Run
    	return int(temp.group(1))

def getAndSortGalaxies(mainPath, folders, mainFolder, timelimit):
    galaxiesList = [[] for i in range(4)]
    insidePath = mainPath+folders+mainFolder
    files = getProcessedFolders(insidePath, 'Run')
    files.sort(key=runSort)  # ordena de Run1 ate RunN-1
    for file in files:
        findNewPosterior = getProcessedFolders(
            insidePath+file, 'new.posterior')

        findCoreErrorFile = getProcessedFolders(insidePath+file, 'core')

        time = 0
        runTimeFilePath = insidePath+file+'/runTime'
        if os.path.isfile(runTimeFilePath):
            f = open(runTimeFilePath, "r")
            lines = f.readlines()
            
            f.close()
            time = lines[2]

        if bool(findNewPosterior):
            galaxiesList[0].append(insidePath + file)
        elif bool(findCoreErrorFile):
            galaxiesList[1].append(insidePath + file)
        elif time != 0 and int(time[0]) >= int(timelimit):
            galaxiesList[2].append(insidePath + file)
        elif not bool(findNewPosterior):
            galaxiesList[3].append(insidePath + file)
    print('Collected from ', folders)
    return galaxiesList

def getParameters(folder, targetFiles, model):
    parameters = {}

    fileChecker = 0 # if any file will be found function will continue
    for file in targerFiles:
        if os.path.exists(folder+file):
            filePath = folder+file
            fileChecker = 1
            break
    
    if fileChecker != 1:
        print('Cant find any useful file in '+folder+' Exiting')
        return parameters



    f = open(filePath, "r")
    lines = f.readlines()
    f.close()
    aux = []
    rhat = []

    checker = 0
    lineCounter = 0
    for line in reversed(open(filePath).readlines()):
        lineCounter=lineCounter+1
        if 'Rhat' in line:
            checker = 1
            break
    
    if checker != 1:
        print('No parameters in '+filePath+' Exiting')
        return parameters

    # define length of parameters segment
    i = -lineCounter # starting where(first line of parameters)
    if model == 'Sersic+Exponential':
        j = i+14 # last line of parameters
        keys = ['X', 'Y', 'MAG', 'BT', 'Rhal', 'DB', 'N', 'Q_disc', 'PA_disc', 'Q_bulge', 'PA_bulge', 'Sky']
    elif model == 'Sersic':
        j = i+10 # last line of parameters
        keys = ['X', 'Y', 'MAG', 'Re', 'N', 'Q','PA','Sky']
    
    while i < j:
        aux.append(lines[i])
        i=i+1

    
    rhat.append( list(zip(*[line.split() for line in aux])) [5])

    for i in range(len(rhat[0])):
        if i != 0 and i != 1:
            parameters[keys[i-2]] = rhat[0][i]

    return parameters

def getErrorGalaxiesStats(errorGalaxies, listCompletePath, totalGalaxies, folders):
    # error lists
    sizeErrorList = []
    devRadErrorList = []
    snrErrorList = []
    skyErrorList = []
    magErrorList = []
    rePixErrorList = []
    nErrorList = []
    qErrorList = []
    paErrorList = []

    # okay lists
    for folder in folders:
        filePath = mainPath+folder+listCompletePath
        with open(filePath) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            processedListCount = totalGalaxies


            for row in csv_reader:
                if line_count > processedListCount:
                    break
                if line_count == 0:
                    line_count += 1
                else:
                    if row[2] in errorGalaxies:
                        magErrorList.append(row[3])
                        rePixErrorList.append(row[4])
                        nErrorList.append(row[5])
                        qErrorList.append(row[6])
                        paErrorList.append(row[7])
                        skyErrorList.append(row[8])
                        sizeErrorList.append(row[9])
                        snrErrorList.append(row[11])
                        devRadErrorList.append(row[14])
                    line_count += 1
    return sizeErrorList, devRadErrorList, snrErrorList, skyErrorList, magErrorList, rePixErrorList, qErrorList, paErrorList

def getMeanForValues(galaxiesList, model):
    galaxyMean = {}

    if model == 'Sersic+Exponential':
        auxX = 0
        auxY = 0
        auxMAG = 0
        auxBT = 0
        auxRhal = 0
        auxDB = 0
        auxN = 0
        auxQDISC = 0
        auxPADISC = 0
        auxQBULGE = 0
        auxPABULGE = 0
        auxSKY = 0

        i = 0
        listLenght = len(galaxiesList)
        for galaxy in galaxiesList:
            if bool(galaxy):
                keys = ['X', 'Y', 'MAG', 'BT', 'Rhal', 'DB', 'N',
                    'Q_disc', 'PA_disc', 'Q_bulge', 'PA_bulge', 'Sky']
                auxX = auxX+float(galaxy['X'])
                auxY = auxY+float(galaxy['Y'])
                auxMAG = auxMAG+float(galaxy['MAG'])
                auxBT = auxBT+float(galaxy['BT'])
                auxRhal = auxRhal+float(galaxy['Rhal'])
                auxDB = auxDB+float(galaxy['DB'])
                auxN = auxN+float(galaxy['N'])
                auxQDISC = auxQDISC+float(galaxy['Q_disc'])
                auxPADISC = auxPADISC+float(galaxy['PA_disc'])
                auxQBULGE = auxQBULGE+float(galaxy['Q_bulge'])
                auxPABULGE = auxPABULGE+float(galaxy['PA_bulge'])
                auxSKY = auxSKY+float(galaxy['Sky'])

                i = i+1

                if i == listLenght:
                    galaxyMean[keys[0]] = auxX / listLenght
                    galaxyMean[keys[1]] = auxY / listLenght
                    galaxyMean[keys[2]] = auxMAG / listLenght
                    galaxyMean[keys[3]] = auxBT / listLenght
                    galaxyMean[keys[4]] = auxRhal / listLenght
                    galaxyMean[keys[5]] = auxDB / listLenght
                    galaxyMean[keys[6]] = auxN / listLenght
                    galaxyMean[keys[7]] = auxQDISC / listLenght
                    galaxyMean[keys[8]] = auxPADISC / listLenght
                    galaxyMean[keys[9]] = auxQBULGE / listLenght
                    galaxyMean[keys[10]] = auxPABULGE / listLenght
                    galaxyMean[keys[11]] = auxSKY / listLenght
            else:
                listLenght = listLenght-1
    elif model == 'Sersic':
        auxX = 0
        auxY = 0
        auxMAG = 0
        auxRe = 0
        auxN = 0
        auxQ = 0
        auxPA = 0
        auxSKY = 0
        
        i = 0
        listLenght = len(galaxiesList)
        for galaxy in galaxiesList:
            if bool(galaxy):
                keys = ['X', 'Y', 'MAG', 'Re', 'N', 'Q','PA','Sky']

                auxX = auxX+float(galaxy['X'])
                auxY = auxY+float(galaxy['Y'])
                auxMAG = auxMAG+float(galaxy['MAG'])
                auxRe = auxRe+float(galaxy['Re'])
                auxN = auxN+float(galaxy['N'])
                auxQ = auxQ+float(galaxy['Q'])
                auxPA = auxPA+float(galaxy['PA'])
                auxSKY = auxSKY+float(galaxy['Sky'])
                
                i=i+1

                if i == listLenght:
                    galaxyMean[keys[0]] = auxX / listLenght
                    galaxyMean[keys[1]] = auxY / listLenght
                    galaxyMean[keys[2]] = auxMAG / listLenght
                    galaxyMean[keys[3]] = auxRe / listLenght
                    galaxyMean[keys[4]] = auxN / listLenght
                    galaxyMean[keys[5]] = auxQ / listLenght
                    galaxyMean[keys[6]] = auxPA / listLenght
                    galaxyMean[keys[7]] = auxSKY / listLenght
            else:
                listLenght=listLenght-1
            
    return galaxyMean

def plotAndSave(means, name, savePath):
    if means:
        plt.bar(range(len(means)), list(means.values()), align='center')
        plt.xticks(range(len(means)), list(means.keys()))
        plt.ylim(bottom=1)
        plt.xticks(rotation=90)

        #titles
        plt.suptitle('Distance from 1 of "' + name+'"', fontsize=18)
        plt.ylabel('Distance from 1', fontsize=14)

        plt.savefig(savePath+'1_'+name+'.png', bbox_inches='tight', figsize=(10,10))
        plt.clf()

def plotAndSaveHistogram(array, name, savePath):
    arrayInt = list(map(float, array))
    plt.suptitle(name, fontsize=18)
    plt.ylabel('Quantity of galaxies', fontsize=14)
    plt.xlabel(name + ' of a Galaxy', fontsize=14)

    #plt.xticks(np.arange(min(arrayInt), max(arrayInt)+1, (max(arrayInt) - min(arrayInt))/10), rotation=90)

    
    plt.hist(arrayInt, density=False, rwidth=0.9, bins=10)
    plt.grid(axis='y', alpha=0.75)
    plt.savefig(savePath + '2_'+name+'Histogram.png', bbox_inches='tight', figsize=[10,5])
    plt.clf()

def getDevRad(path, galaxies, sizeList, devradList):
    path = path + '/listComplete'
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0

        for row in csv_reader:
            if row[2] in galaxies:
                sizeList.append(row[9])
                devradList.append(row[14])
                line_count += 1
            else:
                line_count += 1


def plotAndSaveChart(x, y, name, savePath):

    xFloat = list(map(float, x))
    yFloat = list(map(float, y))
    
    # for i in range(len(x)):
    #     print('X = ', xFloat[i], ' | ', 'Y = ', yFloat[i])



    plt.suptitle(name, fontsize=18)
    plt.ylabel('Processing Time', fontsize=14)
    plt.xlabel(name, fontsize=14)

    #plt.xticks(np.arange(min(arrayInt), max(arrayInt)+1, (max(arrayInt) - min(arrayInt))/10), rotation=90)

    plt.scatter(xFloat, yFloat)
    plt.grid(axis='y', alpha=0.75)
    plt.savefig(savePath + '3_'+name+'.png', bbox_inches='tight', figsize=[10,5])
    plt.clf()

def getTimeList(galaxies):
    timeList = []
    for galaxy in galaxies:
        runTimeFilePath = galaxy+'/runTime'
        if os.path.isfile(runTimeFilePath):
            f = open(runTimeFilePath, "r")
            lines = f.readlines()
            f.close()
            timeList.append(lines[2])
    
    return timeList

# parallel schedulers
def findMeanScheduler(meanList, tooMeanList, step):
    meanList[step] = getMeanForValues(tooMeanList[step], model)
    return meanList[step]

def plotAndSaveScheduler(meanList, namesList, step):
    plotAndSave(meanList[step], namesList[step], savePath)

def getParametersScheduler(parametersList, galaxiesTypeLists, model, step):
    for folder in galaxiesTypeLists[step]:
        parametersList[step].append(getParameters(folder, targerFiles, model))
    return parametersList[step]

def getAndSortGalaxiesScheduler(galaxies, step):
    print('Collection from ', files[step])
    galaxies[step] = getAndSortGalaxies(mainPath, files[step], mainFolder, timelimit)
    return galaxies[step]
# end of funtion segment

mainPath = argv[1] #../test100/processed6/outputGalphatSersic/
# define path to folder with contains output Folder (mainFolder here)
#mainPath = '../processed1/' #OPL1
#mainPath = '/Volumes/MacAux/processados/'

#savePath = 'static/charts/'
savePath = argv[2]
listCompletePath = '/listComplete'

# folder which contains actual catalogs
#mainFolder = '/outputGalphatSersicPlusExp/' # for Sersic+Exponential
mainFolder = '/outputGalphatSersic/' # for Sersic

# put first letters of folders with data you want to filter
folderStarts = 'pro'
#folderStarts = 'cat'

# model definition, Sersic has 7 parameters, and S+E has 12
model = 'Sersic'
#model = 'Sersic+Exponential'

# time limit for current simulation
#timelimit = '10' # S+E
timelimit = '6' # Sersic

# num CPU
numCPU = 4

start = time.time()
files = getProcessedFolders(mainPath, folderStarts)
if folderStarts == 'cat':
    files.sort(key=catalogSort)
elif folderStarts == 'pro':
    files.sort(key=processedSort)
end = time.time()
print('Just collecting folders time = ', end - start)

start = time.time()
#get galaxies
galaxies = [[] for i in range(len(files))]
galaxies = Parallel(n_jobs=numCPU)(delayed(getAndSortGalaxiesScheduler)(galaxies, step) for step in range(len(files)))

okGalaxies = []
galaxiesWithCoreFile = []
galaxiesWithTimelimit = []
errorGalaxies = []

totalNumberOfGalaxies = 0

for galaxy in galaxies:
    for galaxyType in galaxy:
        totalNumberOfGalaxies = totalNumberOfGalaxies + len(galaxyType)
    okGalaxies = okGalaxies + galaxy[0]
    galaxiesWithCoreFile = galaxiesWithCoreFile + galaxy[1]
    galaxiesWithTimelimit = galaxiesWithTimelimit + galaxy[2]
    errorGalaxies = errorGalaxies + galaxy[3]

end = time.time()
print('Runnig time of getAndSortGalaxies (Parallel) = ', end - start)



totalNumberOfGalaxies = len(okGalaxies + galaxiesWithCoreFile + galaxiesWithTimelimit + errorGalaxies)
errorGalaxies = errorGalaxies + galaxiesWithCoreFile + galaxiesWithTimelimit

print('#################################')
print(color.BOLD + color.GREEN + 'Run info' + color.END)
print(color.BOLD + 'Total number of processed galaxies is '  + color.END +str(totalNumberOfGalaxies)+' of which: ')
print(color.BOLD + 'Number of ok galaxies = ' + color.END,len(okGalaxies))
print(color.BOLD + 'Number of core file error galaxies = ' + color.END, len(galaxiesWithCoreFile))
print(color.BOLD + 'Number of timelimit galaxies = ' + color.END, len(galaxiesWithTimelimit))
print(color.BOLD + 'Total number of galaxies with error = ' + color.END, len(errorGalaxies))
print('#################################')


timeLimitIds = []
for file in galaxiesWithTimelimit:
    id = re.search('_(.*)_band_r', file) 
    timeLimitIds.append(id.group(1))

errorIds = []
for file in errorGalaxies:
    id = re.search('_(.*)_band_r', file) 
    errorIds.append(id.group(1))

okGalaxiesIds = []
for file in okGalaxies:
    id = re.search('_(.*)_band_r', file) 
    okGalaxiesIds.append(id.group(1))


sizeErrorList, devRadErrorList, snrErrorList, skyErrorList, magErrorList, rePixErrorList, qErrorList, paErrorList  = getErrorGalaxiesStats(errorIds, listCompletePath, totalNumberOfGalaxies, files)

histogramNames = ['imgSize', 'devRad', 'SNR', 'Sky', 'Mag', 're_pix', 'q', 'pa']
errorLists = [sizeErrorList, devRadErrorList, snrErrorList, skyErrorList, magErrorList, rePixErrorList, qErrorList, paErrorList]


for i in range(len(histogramNames)):
    plotAndSaveHistogram(errorLists[i], histogramNames[i], savePath)

timeList = getTimeList(okGalaxies)
size = []
devrad = []

tempoConvertido = []
for tempo in timeList:
    if tempo[0] != 'N':
        t = tempo.split(':')
        totalMinutos = int(t[0])*60+int(t[1])*1 + int(t[2])/60
        tempoConvertido.append(int(totalMinutos))


for path in files:
    getDevRad(mainPath+path, okGalaxiesIds, size, devrad)


#savePath = 'static/charts/'
chartsNames = ['imgSize', 'devRad']
axis = [devrad, size]
for i in range(len(chartsNames)):
    plotAndSaveChart(tempoConvertido, axis[i], chartsNames[i], savePath)

################################# ALL BELOVE ARE PARALLELIZED!!!

#get parameters
parametersCoreFile = []
parametersTimeout = []
parametersOk = []
parametersError = []

start = time.time()
targerFiles = ['/new2.bie.0.log', '/bie.0.log']
galaxiesTypeLists = [galaxiesWithCoreFile, galaxiesWithTimelimit, okGalaxies, errorGalaxies]
parametersList = [[] for i in range(len(galaxiesTypeLists))]
parametersList = Parallel(n_jobs=numCPU)(delayed(getParametersScheduler)(parametersList, galaxiesTypeLists, model, step) for step in range(len(galaxiesTypeLists)))
end = time.time()
print('Runnig time of getParameters (Parallel) = ', end - start)

# find means
start = time.time()
tooMeanList = [parametersList[0], parametersList[1], parametersList[2], parametersList[3]]
meanList = [[] for i in range(4)]
meanList = Parallel(n_jobs=numCPU)(delayed(findMeanScheduler)(meanList, tooMeanList, step) for step in range(len(tooMeanList)))
end = time.time()
print('Runnig time of getMeanForValues (Parallel) = ', end - start)

#plot graphs
start = time.time()
namesList = ['coreFileErrorGalaxies', 'timeoutGalaxies', 'okGalaxies', 'errorGalaxies']
Parallel(n_jobs=numCPU)(delayed(plotAndSaveScheduler)(meanList, namesList, step) for step in range(len(namesList)))
end = time.time()
print('Runnig time of plotAndSave (Parallel) = ', end - start)



import json
import numpy as np
import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import pandas as pd


#starting here - Helios
with open('jsonDump.txt', 'r') as f:
    data = json.load(f)

# error Galaxies
magListError = []
snrListError = []
skyListError = []
qListError = []

imageSizeListError = []
paListError = []
zeroPointListError = []
nListError = []

rePixListError = []
petroRadpixListError = []

# ok Galaxies
magListOk = []
snrListOk = []
skyListOk = []
qListOk = []
imageSizeListOk = []
paListOk = []
zeroPointListOk = []
nListOk = []

rePixListOk = []
petroRadpixListOk = []

for entry in data:
    if (entry['processingStatus'] == 'Error'):
        magListError.append(entry['galaxy']['mag'])
        snrListError.append(entry['galaxy']['snr'])
        skyListError.append(entry['galaxy']['sky'])
        qListError.append(entry['galaxy']['q'])
        
        imageSizeListError.append(entry['galaxy']['imageSize'])
        paListError.append(entry['galaxy']['pa'])
        zeroPointListError.append(entry['galaxy']['zeroPoint'])
        nListError.append(entry['galaxy']['n'])
        rePixListError.append(entry['galaxy']['rePix'])
        petroRadpixListError.append(entry['galaxy']['petroRadpix'])
    else:
        magListOk.append(entry['galaxy']['mag'])
        snrListOk.append(entry['galaxy']['snr'])
        skyListOk.append(entry['galaxy']['sky'])
        qListOk.append(entry['galaxy']['q'])

        imageSizeListOk.append(entry['galaxy']['imageSize'])
        paListOk.append(entry['galaxy']['pa'])
        zeroPointListOk.append(entry['galaxy']['zeroPoint'])
        nListOk.append(entry['galaxy']['n'])
        rePixListOk.append(entry['galaxy']['rePix'])
        petroRadpixListOk.append(entry['galaxy']['petroRadpix'])


print('ERROR SUCCESS')
print('Mean STD Mean STD')
print('Mag comparison')
print(round(np.mean(magListError),3), round(np.std(magListError),3), '|', round(np.mean(magListOk),3), round(np.std(magListOk),3))
print('--SNR comparison')
print(round(np.mean(snrListError),3), round(np.std(snrListError),3), '|', round(np.mean(snrListOk),3), round(np.std(snrListOk),3))
print('--SKY comparison')
print(round(np.mean(skyListError),3), round(np.std(skyListError),3), '|', round(np.mean(skyListOk),3), round(np.std(skyListOk),3))
print('--Q comparison')
print(round(np.mean(qListError),3), round(np.std(qListError),3), '|', round(np.mean(qListOk),3), round(np.std(qListOk),3))
print('--imageSize comparison')
print(round(np.mean(imageSizeListError),3), round(np.std(imageSizeListError),3), '|', round(np.mean(imageSizeListOk),3), round(np.std(imageSizeListOk),3))
print('--pa comparison')
print(round(np.mean(paListError),3), round(np.std(paListError),3), '|', round(np.mean(paListOk),3), round(np.std(paListOk),3))
print('--zeroPoint comparison')
print(round(np.mean(zeroPointListError),3), round(np.std(zeroPointListError),3), '|', round(np.mean(zeroPointListOk),3), round(np.std(zeroPointListOk),3))
print('--rePix comparison')
print(round(np.mean(rePixListError),3), round(np.std(rePixListError),3), '|', round(np.mean(rePixListOk),3), round(np.std(rePixListOk),3))
print('--petroRadpix comparison')
print(round(np.mean(petroRadpixListError),3), round(np.std(petroRadpixListError),3), '|', round(np.mean(petroRadpixListOk),3), round(np.std(petroRadpixListOk),3))


magErrorDF = pd.Series(magListError, name='magListError')
magOkDF = pd.Series(magListOk, name='magListOk')

snrErrorDF = pd.Series(snrListError, name='snrListError')
snrOkDF = pd.Series(snrListOk, name='snrListOk')

skyErrorDF = pd.Series(skyListError, name='skyListError')
skyOkDF = pd.Series(skyListOk, name='skyListOk')

qErrorDF = pd.Series(qListError, name='qListError')
qOkDF = pd.Series(qListOk, name='qListOk')

imageSizeErrorDF = pd.Series(imageSizeListError, name='imageSizeListError')
imageSizeOkDF = pd.Series(imageSizeListOk, name='imageSizeListOk')

paErrorDF = pd.Series(paListError, name='paListError')
paOkDF = pd.Series(paListOk, name='paListOk')

zeroPointErrorDF = pd.Series(zeroPointListError, name='zeroPointListError')
zeroPointOkDF = pd.Series(zeroPointListOk, name='zeroPointListOk')

rePixErrorDF = pd.Series(rePixListError, name='rePixListError')
rePixOkDF = pd.Series(rePixListOk, name='rePixListOk')

petroRadpixErrorDF = pd.Series(petroRadpixListError, name='petroRadpixListError')
petroRadpixOkDF = pd.Series(petroRadpixListOk, name='petroRadpixListOk')

#df = pd.concat([magErrorDF,magOkDF], axis=1)
#df = pd.concat([snrErrorDF,snrOkDF], axis=1)
#df = pd.concat([skyErrorDF,skyOkDF], axis=1)
#df = pd.concat([qErrorDF,qOkDF], axis=1)
#df = pd.concat([imageSizeErrorDF,imageSizeOkDF], axis=1)
#df = pd.concat([paErrorDF,paOkDF], axis=1)
#df = pd.concat([zeroPointErrorDF,zeroPointOkDF], axis=1)
#df = pd.concat([rePixErrorDF,rePixOkDF], axis=1)
df = pd.concat([petroRadpixErrorDF,petroRadpixOkDF], axis=1)

ax = sns.boxplot(data=df)

plt.show()
import os
import re
from numpy import arange,array,ones
import pandas as pd
import matplotlib.pyplot as plt
import time 
from joblib import Parallel, delayed
import json
import seaborn as sns; sns.set()
import numpy as np

mainPath = '/Volumes/MacAux/processedPolluxFillDB/'
model='Sersic'

errorGalaxies = []
errorIds = []

def getParameters(folder, targetFiles, model):
    parameters = {}

    fileChecker = 0 # if any file will be found function will continue
    for file in targerFiles:
        #print(folder,file)
        if os.path.exists(folder+file):
            filePath = folder+file
            fileChecker = 1
            break
    
    if fileChecker != 1:
        print('Cant find any useful file in '+folder+' Exiting')
        return parameters



    f = open(filePath, "r")
    lines = f.readlines()
    f.close()
    aux = []
    rhat = []

    checker = 0
    lineCounter = 0
    for line in reversed(open(filePath).readlines()):
        lineCounter=lineCounter+1
        if 'Rhat' in line:
            checker = 1
            break
    
    if checker != 1:
        print('No parameters in '+filePath+' Exiting')
        return parameters

    # define length of parameters segment
    i = -lineCounter # starting where(first line of parameters)
    if model == 'Sersic+Exponential':
        j = i+14 # last line of parameters
        keys = ['X', 'Y', 'MAG', 'BT', 'Rhal', 'DB', 'N', 'Q_disc', 'PA_disc', 'Q_bulge', 'PA_bulge', 'Sky']
    elif model == 'Sersic':
        j = i+10 # last line of parameters
        keys = ['X', 'Y', 'MAG', 'Re', 'N', 'Q','PA','Sky']
    
    print(folder)
    while i < j:
        aux.append(lines[i])
        i=i+1

    splitAux = list(zip(*[line.split() for line in aux])) [5]
    print(splitAux)
    
    rhat.append(splitAux)

    for i in range(len(rhat[0])):
        if i != 0 and i != 1:
            parameters[keys[i-2]] = rhat[0][i]

    return parameters

def getMeanForValues(galaxiesList, model):
    galaxyMean = {}

    if model == 'Sersic+Exponential':
        auxX = 0
        auxY = 0
        auxMAG = 0
        auxBT = 0
        auxRhal = 0
        auxDB = 0
        auxN = 0
        auxQDISC = 0
        auxPADISC = 0
        auxQBULGE = 0
        auxPABULGE = 0
        auxSKY = 0

        i = 0
        listLenght = len(galaxiesList)
        for galaxy in galaxiesList:
            if bool(galaxy):
                keys = ['X', 'Y', 'MAG', 'BT', 'Rhal', 'DB', 'N',
                    'Q_disc', 'PA_disc', 'Q_bulge', 'PA_bulge', 'Sky']
                auxX = auxX+float(galaxy['X'])
                auxY = auxY+float(galaxy['Y'])
                auxMAG = auxMAG+float(galaxy['MAG'])
                auxBT = auxBT+float(galaxy['BT'])
                auxRhal = auxRhal+float(galaxy['Rhal'])
                auxDB = auxDB+float(galaxy['DB'])
                auxN = auxN+float(galaxy['N'])
                auxQDISC = auxQDISC+float(galaxy['Q_disc'])
                auxPADISC = auxPADISC+float(galaxy['PA_disc'])
                auxQBULGE = auxQBULGE+float(galaxy['Q_bulge'])
                auxPABULGE = auxPABULGE+float(galaxy['PA_bulge'])
                auxSKY = auxSKY+float(galaxy['Sky'])

                i = i+1

                if i == listLenght:
                    galaxyMean[keys[0]] = auxX / listLenght
                    galaxyMean[keys[1]] = auxY / listLenght
                    galaxyMean[keys[2]] = auxMAG / listLenght
                    galaxyMean[keys[3]] = auxBT / listLenght
                    galaxyMean[keys[4]] = auxRhal / listLenght
                    galaxyMean[keys[5]] = auxDB / listLenght
                    galaxyMean[keys[6]] = auxN / listLenght
                    galaxyMean[keys[7]] = auxQDISC / listLenght
                    galaxyMean[keys[8]] = auxPADISC / listLenght
                    galaxyMean[keys[9]] = auxQBULGE / listLenght
                    galaxyMean[keys[10]] = auxPABULGE / listLenght
                    galaxyMean[keys[11]] = auxSKY / listLenght
            else:
                listLenght = listLenght-1
    elif model == 'Sersic':
        auxX = 0
        auxY = 0
        auxMAG = 0
        auxRe = 0
        auxN = 0
        auxQ = 0
        auxPA = 0
        auxSKY = 0
        
        i = 0
        listLenght = len(galaxiesList)
        for galaxy in galaxiesList:
            if bool(galaxy):
                keys = ['X', 'Y', 'MAG', 'Re', 'N', 'Q','PA','Sky']

                auxX = auxX+float(galaxy['X'])
                auxY = auxY+float(galaxy['Y'])
                auxMAG = auxMAG+float(galaxy['MAG'])
                auxRe = auxRe+float(galaxy['Re'])
                auxN = auxN+float(galaxy['N'])
                auxQ = auxQ+float(galaxy['Q'])
                auxPA = auxPA+float(galaxy['PA'])
                auxSKY = auxSKY+float(galaxy['Sky'])
                
                i=i+1

                if i == listLenght:
                    galaxyMean[keys[0]] = auxX / listLenght
                    galaxyMean[keys[1]] = auxY / listLenght
                    galaxyMean[keys[2]] = auxMAG / listLenght
                    galaxyMean[keys[3]] = auxRe / listLenght
                    galaxyMean[keys[4]] = auxN / listLenght
                    galaxyMean[keys[5]] = auxQ / listLenght
                    galaxyMean[keys[6]] = auxPA / listLenght
                    galaxyMean[keys[7]] = auxSKY / listLenght
            else:
                listLenght=listLenght-1
            
    return galaxyMean

def plotAndSave(means, name):
    if means:
        plt.bar(range(len(means)), list(means.values()), align='center')
        plt.xticks(range(len(means)), list(means.keys()))
        plt.ylim(bottom=1)
        plt.xticks(rotation=90)

        #titles
        plt.suptitle('Distance of each parameter from 1 of "' + name+'"', fontsize=18)
        plt.ylabel('Distance from 1', fontsize=14)

        plt.savefig(name+'.pdf', bbox_inches='tight', figsize=(10,10))
        plt.clf()

def extractor(lists):
    x = []
    y = []
    mag = []
    re = []
    n = []
    q = []
    pa = []
    sky = []
    for list in lists:
        if bool(list):
            #print(list)
            x.append(float(list['X']))
            y.append(float(list['Y']))
            mag.append(float(list['MAG']))
            re.append(float(list['Re']))
            n.append(float(list['N']))
            q.append(float(list['Q']))
            pa.append(float(list['PA']))
            sky.append(float(list['Sky']))

    return x, y, mag, re, n, q, pa, sky

def histogramPlotter(key, value):
    ax = sns.distplot(value, kde=False, rug=True)
    ax.set(title=key, xlabel='Value', ylabel='Data fraction')
    
    ax.xaxis.set_major_locator(plt.MaxNLocator(10))
    
    #limits
    ax.set_xlim(left=0)
    
    plt.savefig(key+'.pdf')
    plt.clf()

#starting here
with open('jsonDump.txt', 'r') as f:
    data = json.load(f)

for entry in data:
    if (entry['processingStatus'] == 'Error'):
        errorGalaxies.append(entry['processingFolder'])
        errorIds.append(entry['galaxy']['objid'])


parametersError = []
targerFiles = ['/new2.bie.0.log', '/bie.0.log']
for folder in errorGalaxies:
    parametersError.append(getParameters(mainPath+folder, targerFiles, model))


errorGalaxyMean = getMeanForValues(parametersError, model)

plotAndSave(errorGalaxyMean, 'error galaxies')


#extracting values from dictionaty
x, y, mag, re, n, q, pa, sky = extractor(parametersError)

xDF = pd.DataFrame({'x': x})
yDF = pd.DataFrame({'y': y})
magDF = pd.DataFrame({'mag': mag})
reDF = pd.DataFrame({'re': re})
nDF = pd.DataFrame({'n': n})
qDF = pd.DataFrame({'q': q})
paDF = pd.DataFrame({'pa': pa})
skyDF = pd.DataFrame({'sky': sky})

# dataComplete = pd.concat([xDF,yDF,magDF,reDF,nDF,qDF,paDF,skyDF], axis=1)

# ax = sns.pairplot(dataComplete)
# plt.savefig('covarience.pdf')
# plt.close()

# values = [xDF,yDF,magDF,reDF,nDF,qDF,paDF,skyDF]
# keys = ['X', 'Y', 'MAG', 'Re', 'N', 'Q','PA','Sky']
# for i in range(len(keys)):
#     histogramPlotter(keys[i],values[i])

# corrMatrix = dataComplete.corr()
# print (corrMatrix)
# sns.heatmap(corrMatrix, annot=True)
# plt.savefig('corelation.pdf')

for galaxy in errorIds:
    print(galaxy)
import json
import numpy as np
import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import pandas as pd

with open('jsonDump.txt', 'r') as f:
    data = json.load(f)

imageSizeListOk = []

for entry in data:
        imageSizeListOk.append(entry['galaxy']['imageSize'])

print('Quantity of galaxies = ', len(imageSizeListOk))

imageSizeOkDF = pd.Series(imageSizeListOk, name='imageSizeListOk')

ax = sns.distplot(imageSizeOkDF, bins = 10, kde=False)
ax.set(title='Distribution os galaxy sizes in our data sample', xlabel='Galaxy Size (pixels)')
ax.xaxis.set_major_locator(plt.MaxNLocator(10))
ax.yaxis.set_major_locator(plt.MaxNLocator(12))

#plt.show()
plt.savefig('galaxySize.pdf')
plt.clf()
$(document).ready(function () {
  var table = $('#table_id').DataTable(
    {
      select: {
        style: 'multi'
      },
      columnDefs:
        [
          // ahref
          {
            targets: [2],
            data: "download_link",
            render: function (data, type, row, meta) {
              return '<a href="' + data + '" class="btn btn-primary">Img</a>';
            }
          },
          {
            targets: [7],
            data: "download_link",
            render: function (data, type, row, meta) {
              return '<a href="' + data + '" class="btn btn-primary">Posterior file</a>';
            }
          },
          {
            targets: [8],
            data: "id",
            render: function (data, type, row, meta) {
              return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#' + data + '">Corner Plot</button>';
            }
          },
          // styles
          {
            "targets": 6,
            "createdCell": function (td, cellData, rowData, row, col) {
              if (cellData == 'Error') {
                $(td).css('color', 'red').css('font-size', '24px').css('text-align', 'center').css('vertical-align', 'middle')
              }
              else {
                $(td).css('color', 'blue').css('font-size', '24px').css('text-align', 'center').css('vertical-align', 'middle')
              }
            }
          },
          {
            "targets": [0, 1, 2, 3, 4, 5, 7, 8],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).css('text-align', 'center').css('vertical-align', 'middle')
            }
          }
        ],
      ajax: {
        url: '/static/data/jsonDump.txt',
        dataSrc: '',
      },
      columns: [
        { data: "id" },
        { data: "galaxy.objid" },
        { 
          data: 'galaxy',
          render: function (data, type, row, meta) {
            return '<a href="' + data.imgPath + '" class="btn btn-primary">Img</a> <a href="' + data.maskPath + '" class="btn btn-primary">Mask</a>'
          }
        },
        { data: "processingTime" },
        { data: "galaxy.imageSize" },
        { data: "galaxy.devRad" },
        { data: "processingStatus" },
        { data: "posterior" },
        { data: "cornerPlot" },
      ],
      deferRender: true,
      processing: true,
      stateSave: true,
      deferRender: true,
      scrollCollapse: true,
      scroller: true,
      stateSaveCallback: function (settings, data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data))
      },
      stateLoadCallback: function (settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance))
      }
    },
  );

  $('#example tbody').on( 'click', 'tr', function () {
    $(this).toggleClass('selected');
  } );

  $('#button').click( function () {
      // alert( table.rows('.selected').data().length +' row(s) selected' );
      var ids = $.map(table.rows('.selected').data(), function (item) {
        return item[0]
      });
      console.log(ids)
      alert(table.rows('.selected').data().length + ' row(s) selected');

  } );



  $('#table_id tbody').on('click', 'button', function () {
    var d = table.row( $(this).parents('tr') ).data();

    var name = $('td', this).eq(1).text();
    $('#myImg').attr('src', d.cornerPlot).attr('alt', d.cornerPlot)
    $('#saveForm').attr('action', d.cornerPlot)
    $('#titleHeader').html('Corner Plot for ' + d.galaxy.objid + ' ('+ d.processingStatus +')')
    $('#DescModal').modal("show");
  });
});
from flask import render_template, flash, redirect, url_for, send_file, request, jsonify
from app import app, db
from app.forms import LoginForm, UploadForm
from werkzeug import secure_filename
from app.models import Galaxy, ProcessedSersicPlusExponential, ProcessedSersic, GalaxySchema, ProcessedSersicSchema, ProcessedSersicPlusExponentialSchema
from statistics import mean
import json

galaxySchema = GalaxySchema(many=True)
processedSersicSchema = ProcessedSersicSchema(many=True)
processedSersicPlusExponentialSchema= ProcessedSersicPlusExponentialSchema(many=True)

# routine that generates json file from app.db file in root
@app.route('/json', methods=['GET'])
def jsonTest():
    galaxies = ProcessedSersic.query.all() #sersic
    #galaxies = ProcessedSersicPlusExponential.query.all() #se
    #galaxies = ProcessedSersic.query.filter(ProcessedSersic.id < 2000).all() # test query
    result = processedSersicSchema.dumps(galaxies) # sersic
    #result = processedSersicPlusExponentialSchema.dumps(galaxies) # se
    
    
    # to do -> DUMP STATS like dict
    totalErrors = 0
    tempoConvertido = []
    tamanhos = []
    devrads = []
    totalErrors = 0
    for galaxy in galaxies:
        tamanhos.append(galaxy.galaxy.imageSize)
        devrads.append(galaxy.galaxy.devRad)
        if galaxy.processingTime[0] != 'N' and galaxy.processingStatus == 'Success':
            t = galaxy.processingTime.split(':')
            totalMinutos = int(t[0])*60+int(t[1])*1 + int(t[2])/60
            tempoConvertido.append(int(totalMinutos))

    tempoMedio = mean(tempoConvertido)
    tamanhoMedio = mean(tamanhos)
    devRadMedio = mean(devrads)

    totalGalaxies = len(galaxies)
    for galaxy in galaxies:
        if galaxy.processingStatus == 'Error':
            totalErrors = totalErrors + 1

    stats = {}
    stats['tempoMedio'] = tempoMedio
    stats['tamanhoMedio'] = tamanhoMedio
    stats['devRadMedio'] = devRadMedio
    stats['totalGalaxies'] = totalGalaxies
    stats['totalErrors'] = totalErrors

    with open('app/static/data/stats.txt', 'w') as outfile:
        json.dump(stats, outfile)
    # dumping json db registry
    with open('app/static/data/jsonDump.txt', 'w') as filehandle:
        filehandle.write(result)

    return result

# main list access
@app.route('/index/<model>')
def jsonIndex(model):
    with open('app/static/data/stats.txt') as json_file:
        stats = json.load(json_file)

    return render_template('index.html', model=model, totalErrors=stats['totalErrors'], totalGalaxies=stats['totalGalaxies'], tempoMedio=stats['tempoMedio'], tamanhoMedio=stats['tamanhoMedio'], devRadMedio=stats['devRadMedio'])

@app.route('/dbIndex/<model>')
def index(model):
    user = {'username': 'Igor'}

    totalErrors = 0

    galaxies = Galaxy.query.all()
    if model == 'sersic':
        processedGalaxiesAll = ProcessedSersic.query.all()
        #processedGalaxiesAll = ProcessedSersic.query.filter(ProcessedSersic.id < 1000).all() # test query

        # transfer this bit to collect?
        # convertende tempo para poder calcular media
        tempoConvertido = []
        tamanhos = []
        devrads = []
        for galaxy in processedGalaxiesAll:
            tamanhos.append(galaxy.galaxy.imageSize)
            devrads.append(galaxy.galaxy.devRad)
            if galaxy.processingTime[0] != 'N' and galaxy.processingStatus == 'Success':
                t = galaxy.processingTime.split(':')
                totalMinutos = int(t[0])*60+int(t[1])*1 + int(t[2])/60
                tempoConvertido.append(int(totalMinutos))

        tempoMedio = mean(tempoConvertido)
        tamanhoMedio = mean(tamanhos)
        devRadMedio = mean(devrads)

        totalGalaxies = len(processedGalaxiesAll)
        for galaxy in processedGalaxiesAll:
            if galaxy.processingStatus == 'Error':
                totalErrors = totalErrors + 1
        
        
        return render_template('dbIndex.html', title='Home', model=model, user=user, galaxies=galaxies, processedGalaxies=processedGalaxiesAll, totalErrors=totalErrors, totalGalaxies=totalGalaxies, tempoMedio=tempoMedio, tamanhoMedio=tamanhoMedio, devRadMedio=devRadMedio)
    
    elif model == 'sersicexponential':
        processedGalaxiesAll = ProcessedSersicPlusExponential.query.all()
        if processedGalaxiesAll:

            # transfer this bit to collect?
            # convertende tempo para poder calcular media
            tempoConvertido = []
            tamanhos = []
            devrads = []
            for galaxy in processedGalaxiesAll:
                tamanhos.append(galaxy.galaxy.imageSize)
                devrads.append(galaxy.galaxy.devRad)
                if galaxy.processingTime[0] != 'N' and galaxy.processingStatus == 'Success':
                    t = galaxy.processingTime.split(':')
                    totalMinutos = int(t[0])*60+int(t[1])*1 + int(t[2])/60
                    tempoConvertido.append(int(totalMinutos))

            tempoMedio = mean(tempoConvertido)
            tamanhoMedio = mean(tamanhos)
            devRadMedio = mean(devrads)

            totalGalaxies = len(processedGalaxiesAll)
            for galaxy in processedGalaxiesAll:
                if galaxy.processingStatus == 'Error':
                    totalErrors = totalErrors + 1
            
            if int(totalGalaxies%app.config['POSTS_PER_PAGE']) == 0:
                totalPages = int(totalGalaxies/app.config['POSTS_PER_PAGE'])
            else:
                totalPages = int(totalGalaxies/app.config['POSTS_PER_PAGE']) + 1

            page = request.args.get('page', 1, type=int)
            processedGalaxies = ProcessedSersicPlusExponential.query.paginate(page, app.config['POSTS_PER_PAGE'], False)
            next_url = url_for('index', page=processedGalaxies.next_num) \
                if processedGalaxies.has_next else None
            prev_url = url_for('index', page=processedGalaxies.prev_num) \
                if processedGalaxies.has_prev else None
            
            return render_template('dbIndex.html', title='Home', model=model, user=user, page=page, totalPages=totalPages, galaxies=galaxies, processedGalaxies=processedGalaxies.items, next_url=next_url, prev_url=prev_url, totalErrors=totalErrors, totalGalaxies=totalGalaxies, tempoMedio=tempoMedio, tamanhoMedio=tamanhoMedio, devRadMedio=devRadMedio)
        else:
            return render_template('noGalaxies.html')

@app.route('/pagination/<model>')
def paginationVersion(model):
    user = {'username': 'Igor'}

    totalErrors = 0

    galaxies = Galaxy.query.all()
    if model == 'sersic':
        #processedGalaxiesAll = ProcessedSersic.query.all()
        processedGalaxiesAll = ProcessedSersic.query.filter(ProcessedSersic.id < 24000).all() # test query

        # transfer this bit to collect?
        # convertende tempo para poder calcular media
        tempoConvertido = []
        tamanhos = []
        devrads = []
        for galaxy in processedGalaxiesAll:
            tamanhos.append(galaxy.galaxy.imageSize)
            devrads.append(galaxy.galaxy.devRad)
            if galaxy.processingTime[0] != 'N' and galaxy.processingStatus == 'Success':
                t = galaxy.processingTime.split(':')
                totalMinutos = int(t[0])*60+int(t[1])*1 + int(t[2])/60
                tempoConvertido.append(int(totalMinutos))

        tempoMedio = mean(tempoConvertido)
        tamanhoMedio = mean(tamanhos)
        devRadMedio = mean(devrads)

        totalGalaxies = len(processedGalaxiesAll)
        for galaxy in processedGalaxiesAll:
            if galaxy.processingStatus == 'Error':
                totalErrors = totalErrors + 1
        
        if int(totalGalaxies%app.config['POSTS_PER_PAGE']) == 0:
            totalPages = int(totalGalaxies/app.config['POSTS_PER_PAGE'])
        else:
            totalPages = int(totalGalaxies/app.config['POSTS_PER_PAGE']) + 1

        page = request.args.get('page', 1, type=int)

        #processedGalaxies = ProcessedSersic.query.paginate(page, app.config['POSTS_PER_PAGE'], False)
        processedGalaxies = ProcessedSersic.query.filter(ProcessedSersic.id < 24000).paginate(page, app.config['POSTS_PER_PAGE'], False) # test query
        next_url = url_for('paginationVersion', model=model, page=processedGalaxies.next_num) if processedGalaxies.has_next else 'None'
        prev_url = url_for('paginationVersion', model=model,page=processedGalaxies.prev_num) if processedGalaxies.has_prev else 'None'
        
        return render_template('paginationIndex.html', title='Home', model=model, user=user, page=page, totalPages=totalPages, galaxies=galaxies, processedGalaxies=processedGalaxies.items, next_url=next_url, prev_url=prev_url, totalErrors=totalErrors, totalGalaxies=totalGalaxies, tempoMedio=tempoMedio, tamanhoMedio=tamanhoMedio, devRadMedio=devRadMedio)
    
    elif model == 'sersicexponential':
        processedGalaxiesAll = ProcessedSersicPlusExponential.query.all()
        if processedGalaxiesAll:

            # transfer this bit to collect?
            # convertende tempo para poder calcular media
            tempoConvertido = []
            tamanhos = []
            devrads = []
            for galaxy in processedGalaxiesAll:
                tamanhos.append(galaxy.galaxy.imageSize)
                devrads.append(galaxy.galaxy.devRad)
                if galaxy.processingTime[0] != 'N' and galaxy.processingStatus == 'Success':
                    t = galaxy.processingTime.split(':')
                    totalMinutos = int(t[0])*60+int(t[1])*1 + int(t[2])/60
                    tempoConvertido.append(int(totalMinutos))

            tempoMedio = mean(tempoConvertido)
            tamanhoMedio = mean(tamanhos)
            devRadMedio = mean(devrads)

            totalGalaxies = len(processedGalaxiesAll)
            for galaxy in processedGalaxiesAll:
                if galaxy.processingStatus == 'Error':
                    totalErrors = totalErrors + 1
            
            if int(totalGalaxies%app.config['POSTS_PER_PAGE']) == 0:
                totalPages = int(totalGalaxies/app.config['POSTS_PER_PAGE'])
            else:
                totalPages = int(totalGalaxies/app.config['POSTS_PER_PAGE']) + 1

            page = request.args.get('page', 1, type=int)
            processedGalaxies = ProcessedSersicPlusExponential.query.paginate(page, app.config['POSTS_PER_PAGE'], False)
            next_url = url_for('index', page=processedGalaxies.next_num) \
                if processedGalaxies.has_next else None
            prev_url = url_for('index', page=processedGalaxies.prev_num) \
                if processedGalaxies.has_prev else None
            
            return render_template('paginationIndex.html', title='Home', model=model, user=user, page=page, totalPages=totalPages, galaxies=galaxies, processedGalaxies=processedGalaxies.items, next_url=next_url, prev_url=prev_url, totalErrors=totalErrors, totalGalaxies=totalGalaxies, tempoMedio=tempoMedio, tamanhoMedio=tamanhoMedio, devRadMedio=devRadMedio)
        else:
            return render_template('noGalaxies.html')


@app.route('/login')
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)

# funcaoo que retorna qualquer objeto com seu caminho inteiro no nome
@app.route('/index/<path:id>')
def returnImg(id):
    print(id)
    fileName = id.rsplit('/', 1)[-1]
    return send_file(id, as_attachment=True, attachment_filename=fileName)

# funcaoo que retorna qualquer objeto com seu caminho inteiro no nome
@app.route('/index/galaxy/<id>')
def galaxyPage(id):
    return render_template('galaxy.html', title='Galaxy', id=id)

# funcaoo que retorna qualquer objeto com seu caminho inteiro no nome
@app.route('/pagination/<path:id>')
def returnImgPg(id):
    print(id)
    fileName = id.rsplit('/', 1)[-1]
    return send_file(id, as_attachment=True, attachment_filename=fileName)

# funcaoo que retorna qualquer objeto com seu caminho inteiro no nome
@app.route('/pagination/galaxy/<id>')
def galaxyPagePg(id):
    return render_template('galaxy.html', title='Galaxy', id=id)

# funcaoo que retorna qualquer objeto com seu caminho inteiro no nome
@app.route('/<path:id>')
def returnImgJSON(id):
    print(id)
    fileName = id.rsplit('/', 1)[-1]
    return send_file(id, as_attachment=True, attachment_filename=fileName)